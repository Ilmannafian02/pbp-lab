from django.db import models

# Create your models here.
class Note(models.Model):
    to_message = models.CharField(max_length=30)
    from_message = models.CharField(max_length=30)
    title_message = models.CharField(max_length=30)
    message = models.CharField(max_length=30)