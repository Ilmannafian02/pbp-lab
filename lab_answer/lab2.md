## 1. Apakah perbedaan antara JSON dan XML?
1. Secara bahasa, XML menggunakan bahasa markup, yang memiliki tag untuk mendefinisikan elemen di dalamnya, dan kemudian disimpan sebagai tree structure, sementara JSON menggunakan bahasa pemrograman javascript yang disimpan seperti map dengan pasangan key value.

2. XML berorientasi pada dokumen, sementara JSON berorientasi pada data.

3. JSON lebih mudah dibaca, sementara XML lebih rumit

4. JSON mendukung array, XML tidak

## 2. Apakah perbedaan antara HTML dan XML?
1. Fungsi XML lebih kepada transfer data, sementara HTML menyajikan data

2. XML case-sensitive, sementara HTML case-insensitive, sehingga penggunaan tag pada XML lebih ketat

3. XML mendukung namespaces sementara HTML tidak

4. Tag penutup lebih strict di XML daripada HTML

5. Tag di HTML lebih terbatas daripada XML
## Referensi
1. [Perbedaan Antara JSON dan XML](https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html)
2. [Apa Perbedaan JSON Dan XML?](https://www.monitorteknologi.com/perbedaan-json-dan-xml/)
3. [Apa Saja Perbedaan XML dan HTML](https://blogs.masterweb.com/perbedaan-xml-dan-html/)
4. [Perbedaan XML dan HTML: Fitur dan Kuncinya](https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html)
