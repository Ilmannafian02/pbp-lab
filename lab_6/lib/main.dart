import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Testing Widget Flutter"),
          backgroundColor: Colors.red,
          actions: <Widget>[
            IconButton(icon: Icon(Icons.camera_alt), onPressed: () => {}),
            IconButton(icon: Icon(Icons.account_circle), onPressed: () => {})
          ],
        ),
        body: Center(
            child: Column(children: <Widget>[
              
          Container(
              margin: EdgeInsets.all(25),
              child: TextButton(
                child: Text(
                  'Testing Button Flutter',
                  style: TextStyle(fontSize: 20.0),
                ),
                onPressed: () {},
              ))
        ])),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.save),
          backgroundColor: Colors.red,
          foregroundColor: Colors.white,
          onPressed: () => {},
        ),
      ),
    );
  }
}

