from django.urls import path
from .views import add_friend

urlpatterns = [
    path('', add_friend, name='add_friend'),
    path('add', add_friend, name='add_friend')

    # TODO Add friends path using friend_list Views
]
