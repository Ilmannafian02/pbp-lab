from django.shortcuts import render
from .forms import FriendForm

def add_friend(request):
    context = {}
    form = FriendForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        form.save()
    
    context['form'] = form
    return render(request, "lab3_form.html", context)