from django.db import models

# Create your models here.
class Friend(models.Model):
    friend_name = models.CharField(max_length=30)
    friend_npm = models.CharField(max_length=30)
    dob = models.DateField()

    def __str__(self):
        return self.friend_name, self.friend_npm