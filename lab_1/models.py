from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here

class Friend(models.Model):
    friend_name = models.CharField(max_length=30)
    friend_npm = models.CharField(max_length=30)
    dob = models.DateField()
    # TODO Implement missing attributes in Friend model
