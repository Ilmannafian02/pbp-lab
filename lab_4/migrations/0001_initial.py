# Generated by Django 3.2.7 on 2021-10-11 08:41

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Note',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('to_message', models.CharField(max_length=30)),
                ('from_message', models.CharField(max_length=30)),
                ('title_message', models.CharField(max_length=30)),
                ('message', models.CharField(max_length=30)),
            ],
        ),
    ]
